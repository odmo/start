import React from "react"
import { button } from 'react-bootstrap';

import Image from '../components/Image';
import LayoutBasic from '../layouts/LayoutBasic'
import Seo from '../components/Seo';
import { ReactComponent as IcYoutube } from '../images/svg/youtube.svg';

import "./index.scss";

export default function IndexPage (){
  return(
    <LayoutBasic> 
      <Seo title="Home"/>
      <h2 className = "title" > Estamos en la home</h2>
      <button type="button" className="btn btn-danger">Danger</button>
      {/* <Image fileName = "gatsby-icon.png" alt="logo" />
      <Image fileName = "gatsby-astronaut.png" alt="Austronaut" /> */}
      <Image fileName = "cumplesusy.png" alt="Austronaut" />
      <IcYoutube/>
    </LayoutBasic>
  )
}
