import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import { useStaticQuery, graphql } from 'gatsby';

export default function Seo(props) 
{
    const { title, description, meta, lang }  = props;
    
    const data = useStaticQuery( graphql`
        query{
            site{
                siteMetadata{
                    title
                    description
                    author
                }
            }
        }
    `)

    const metaDescription = description || data.site.siteMetadata.description;
    
    return (
        <Helmet 
            htmlAttributes = {{
                lang
            }} 
            title= {title}
            titleTemplate = { `%s | ${data.site.siteMetadata.title}`}
            meta = {[
                {
                    name : 'description',
                    content : metaDescription
                },
                {
                    meta : 'og:title',
                    content : title
                },
                {
                    name : 'og:description',
                    content : metaDescription
                },
                {
                    name : 'og:type',
                    content : "website"
                },
                {
                    name : 'twiter:card',
                    content : 'summary'
                },
                {
                    name : 'twiter:creator',
                    content : data.site.siteMetadata.author
                },
                {
                    name : 'twiter:title',
                    content : title
                },
                {
                    name : 'twiter:description',
                    content : metaDescription
                },
            ].concat(meta)}
        />
    )
}

Seo.defaultProps = {
    lang : "es",
    meta : [],
    description : ""
}

Seo.propTypes = {
    title : PropTypes.string.isRequired,
    description : PropTypes.string,
    lang : PropTypes.string,
    meta : PropTypes.arrayOf( PropTypes.object)
}